$(document).ready(function() {
    $('.edit').hide()
    $('.cv').click(function() {
        $('.edit').show();


    })

    $('.web-development-cv').click(function(e) {
        var offset = $(this).offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top
            // alert(e.pageX - offset.left);
            // alert(e.pageY - offset.top);

        // alert(x);
        // alert(y);

        if (x > 38 && x < 285 && y > 10 && y < 159) {
            $('.tips-section').html('Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. Tailor the CV according to job you’re applying for. There is no ‘one size fits all’ CV – so always adapt it before applying to a job. Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.');
        }

        if (x > 298 && x < 399 && y > 26 && y < 123) {
            $('.tips-section').html('Your photo should be professional. It\'s better to send a CV without a photo, than with a one that makes you seem unserious.');
        }

        if (x > 510 && x < 678 && y > 12 && y < 130) {
            $('.tips-section').html('Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. If you have proof of your previous job experiences online, link that too.');
        }

        if (x > 38 && x < 399 && y > 166 && y < 980) {
            $('.tips-section').html('Write your previous <span style="font-weight:bold">job experience</span> and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.<br><br> <span style="font-weight:bold">Showcase your tech stack</span>. List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub.<br><br> <span style="font-weight:bold">Use numbers</span>. If you helped a website scale, put a metric in there.<br><br> Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last.<br><br> Don’t be afraid to include a “Informal Education” section, where you can list all the courses, podcasts and webinars that you used to teach yourself about software development. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.');
        }

        if (x > 400 && x < 675 && y > 167 && y < 916) {
            $('.tips-section').html('<span style="font-weight:bold">Education</span> is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school.<br><br> All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.');
        }

    });

    $('.data-science-cv').click(function(e) {
        var offset = $(this).offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top
            // alert(e.pageX - offset.left);
            // alert(e.pageY - offset.top);

        // alert(x);
        // alert(y);

        if (x > 38 && x < 285 && y > 10 && y < 159) {
            $('.tips-section').html('Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. Tailor the CV according to job you’re applying for. There is no ‘one size fits all’ CV – so always adapt it before applying to a job. Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.');
        }

        if (x > 1.5 && x < 129 && y > 6 && y < 130) {
            $('.tips-section').html('Your photo should be professional. It\'s better to send a CV without a photo, than with a one that makes you seem unserious.');
        }

        if (x > 186 && x < 507 && y > 139 && y < 171) {
            $('.tips-section').html('Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. If you have proof of your previous job experiences online, link that too.');
        }

        if (x > 38 && x < 399 && y > 166 && y < 600) {
            $('.tips-section').html('Write your previous <span style="font-weight:bold">job experience</span> and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.<br><br> <span style="font-weight:bold">Showcase your tech stack</span>. List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub.<br><br> <span style="font-weight:bold">Use numbers</span>. If you helped a website scale, put a metric in there.<br><br> Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last.<br><br> ');
        }

        if (x > 7 && x < 328 && y > 603 && y < 784) {
            $('.tips-section').html('<span style="font-weight:bold">Education</span> is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school.<br><br>');
        }

        if (x > 362 && x < 668 && y > 459 && y < 530) {
            $('.tips-section').html('All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.');
        }
        if (x > 362 && x < 668 && y > 539 && y < 651) {
            $('.tips-section').html('Don’t be afraid to include a “Informal Education” section, where you can list all the courses, podcasts and webinars that you used to teach yourself about software development. Don’t include everything – just the education that is relevant and necessary for the position you are applying for.');
        }
    });

    $('.digital-marketing-cv').click(function(e) {
        var offset = $(this).offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top
            // alert(e.pageX - offset.left);
            // alert(e.pageY - offset.top);

        // alert(x);
        // alert(y);

        if (x > 38 && x < 285 && y > 10 && y < 159) {
            $('.tips-section').html('Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. Tailor the CV according to job you’re applying for. There is no ‘one size fits all’ CV – so always adapt it before applying to a job. Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.');
        }

        if (x > 1.5 && x < 129 && y > 6 && y < 130) {
            $('.tips-section').html('Your photo should be professional. It\'s better to send a CV without a photo, than with a one that makes you seem unserious.');
        }

        if (x > 186 && x < 507 && y > 139 && y < 171) {
            $('.tips-section').html('Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. If you have proof of your previous job experiences online, link that too.');
        }

        if (x > 38 && x < 399 && y > 166 && y < 600) {
            $('.tips-section').html('Write your previous <span style="font-weight:bold">job experience</span> and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.<br><br> <span style="font-weight:bold">Showcase your tech stack</span>. List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub.<br><br> <span style="font-weight:bold">Use numbers</span>. If you helped a website scale, put a metric in there.<br><br> Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last.<br><br> ');
        }

        if (x > 7 && x < 328 && y > 603 && y < 784) {
            $('.tips-section').html('<span style="font-weight:bold">Education</span> is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school.<br><br>');
        }

        if (x > 362 && x < 668 && y > 459 && y < 530) {
            $('.tips-section').html('All languages, even those you don’t speak fluently, can help you get a certain position. It’s good to use expressions such as “fluent in”, “speaking level of”, “native language” and similar.');
        }

    });

    $('.design-cv').click(function(e) {
        var offset = $(this).offset();
        var x = e.pageX - offset.left;
        var y = e.pageY - offset.top
            // alert(e.pageX - offset.left);
            // alert(e.pageY - offset.top);

        // alert(x);
        // alert(y);

        if (x > 23 && x < 406 && y > 10 && y < 374) {
            $('.tips-section').html('Write your resume in the language that you would use at your workplace. This is a great chance to show the company’s representatives that you can express yourself. Tailor the CV according to job you’re applying for. There is no ‘one size fits all’ CV – so always adapt it before applying to a job. Write a short intro that truly represents you –not cheesy quotes, but something you truly believe in. Don’t put overused buzzwords, describe yourself and your capabilities with your own vocabulary.');
        }

        if (x > 46 && x < 331 && y > 786 && y < 883) {
            $('.tips-section').html('Email, phone number and date of birth are the practical part of the assembly of a CV. Enter your professional email address, preferably on Gmail. Your city of living is an information that interests your potential employers. But don’t leave your home address. Leave a link to your LinkedIn profile. If you have proof of your previous job experiences online, link that too.');
        }

        if (x > 46 && x < 331 && y > 426 && y < 766) {
            $('.tips-section').html('Write your previous <span style="font-weight:bold">job experience</span> and if you’re currently working, your current job titles. Explain your responsibilities and projects and list the tech stack and products/projects you worked on. If you were working in a team, let the recruiter know what your role was.<br><br> <span style="font-weight:bold">Showcase your tech stack</span>. List the stack under every previous project you have worked on, or showcase your projects on GitHub. The easiest way to do this is to list the stack under a previous project that you have worked on. Another way is to showcase your projects on GitHub.<br><br> <span style="font-weight:bold">Use numbers</span>. If you helped a website scale, put a metric in there.<br><br> Dates – a time frame in which you have been on a certain job title. If you are still working there, leave a hyphen ( - ). Leave your most recent experience first, and the oldest ones last.<br><br> ');
        }

        if (x > 387 && x < 644 && y > 426 && y < 579) {
            $('.tips-section').html('<span style="font-weight:bold">Education</span> is an important section – but be sure to select only what is relevant to the job you’re applying for. Write about your university degrees, relevant courses and if you have any publications, but skip the high and elementary school.<br><br>');
        }

        if (x > 387 && x < 644 && y > 604 && y < 774) {
            $('.tips-section').html('List all the <span style="font-weight:bold">skills, tools and technologies</span> that you know well.');
        }

    });

    $('div.experice:hidden').on('click', function() {
        var x = document.getElementById("second");
        console.log(x);
        if (window.getComputedStyle(x).visibility === "hidden") {
            // Do something..
            $('.tips-section').html('tessssssss');
        }
    })


    //edit cv form and save
    // $(".editable").each(function(i) {
    //     var $this = $(this);
    //     $this.attr("id", "orig-" + i);

    //     var $edit = $("<button />")
    //         .text("edit")
    //         .attr("id", "update-" + i)
    //         .click(function() {
    //             var $input = $('<input type="text" />')
    //                 .attr("id", "edit" + i)
    //                 .val($this.text());

    //             var $save = $('<button>Save</button>')
    //                 .click(function() {
    //                     var $new = $("<p />").text($input.val());
    //                     $input.replaceWith($new);
    //                     $(this).hide();
    //                 });
    //             $(this).replaceWith($save);

    //             $this.replaceWith($input);
    //         });

    //     $(this).after($edit)
    // });

    $('body').on('click', '[data-editable]', function() {

        var $el = $(this);

        // console.log($el.text());

        var $input = $('<input/>').val($el.text());
        $el.replaceWith($input);
        // console.log($input);
        var save = function() {
            var $p = $($el).text($input.val());
            $input.replaceWith($p);
        };

        /**
          We're defining the callback with `one`, because we know that
          the element will be gone just after that, and we don't want 
          any callbacks leftovers take memory. 
          Next time `p` turns into `input` this single callback 
          will be applied again.
        */
        $input.one('blur', save).focus();

    });

    //add skill
    $('body').on('click', '[skill-editable]', function() {

        // $('.add-skill').after('<input/>');
        // var $span = $('<span>');
        // console.log($el);
        var $inputField = $('<input/>').addClass('proba');
        $('.skill').after($inputField);
        // console.log($inputField);

        // var $input = $('<input/>').val($el.text());

        // // console.log($input);
        var save = function() {
            var $inputContent = $($inputField).val();

            // $inputField.after($inputContent);
            $('.skill').after($inputContent).text();
        };

        /**
          We're defining the callback with `one`, because we know that
          the element will be gone just after that, and we don't want 
          any callbacks leftovers take memory. 
          Next time `p` turns into `input` this single callback 
          will be applied again.
        */
        $('.skill').one('blur', save).focus();

    });

    //photo upload
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.profile-pic').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $(".file-upload").on('change', function() {
        readURL(this);
    });

    $(".upload-button").on('click', function() {
        console.log("file-button");

        $(".file-upload").click();
    });


    //download pdf
    // $('#GetFile').on('click', function() {
    //     let x = document.getElementById('first-page');
    //     console.log(x);


    //     $.ajax({
    //         url: 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/172905/test.pdf',
    //         method: 'GET',
    //         crossDomain: true,
    //         xhrFields: {
    //             responseType: 'blob'
    //         },
    //         success: function(data) {
    //             var a = document.createElement('a');
    //             var url = window.URL.createObjectURL(data);
    //             a.href = url;
    //             a.download = 'myfile.pdf';
    //             document.body.append(a);
    //             a.click();
    //             a.remove();
    //             window.URL.revokeObjectURL(url);
    //         }
    //     });
    // });

    //download without content
    // var doc = new jsPDF();
    // var specialElementHandlers = {
    //     '#editor': function(element, renderer) {
    //         return true;
    //     }
    // };

    // margins = {
    //     top: 72,
    //     bottom: 1589,
    //     left: 89,
    //     width: 768
    // };

    // $('#GetFile').click(function() {
    //     doc.fromHTML($('#content').html(), 89, 72, {
    //         'width': 768,
    //         'elementHandlers': specialElementHandlers
    //     });
    //     doc.save('sample-file.pdf');
    // });

    //vtor primer dobar
    // function demoFromHTML() {
    //     var pdf = new jsPDF('p', 'pt', 'letter');
    //     // source can be HTML-formatted string, or a reference
    //     // to an actual DOM element from which the text will be scraped.
    //     source = $('#content')[0];

    //     // we support special element handlers. Register them with jQuery-style 
    //     // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    //     // There is no support for any other type of selectors 
    //     // (class, of compound) at this time.
    //     specialElementHandlers = {
    //         // element with id of "bypass" - jQuery style selector
    //         '#getFile': function(element, renderer) {
    //             // true = "handled elsewhere, bypass text extraction"
    //             return true
    //         }
    //     };
    //     margins = {
    //         top: 72,
    //         bottom: 1589,
    //         left: 89,
    //         width: 768
    //     };
    //     // all coords and widths are in jsPDF instance's declared units
    //     // 'inches' in this case
    //     pdf.fromHTML(
    //         source, // HTML string or DOM elem ref.
    //         margins.left, // x coord
    //         margins.top, { // y coord
    //             'width': margins.width, // max width of content on PDF
    //             'elementHandlers': specialElementHandlers
    //         },

    //         function(dispose) {
    //             // dispose: object with X, Y of the last line add to the PDF 
    //             //          this allow the insertion of new lines after html
    //             pdf.save('Test.pdf');
    //         }, margins
    //     );
    // }


    //tret obid
    // function demoFromHTML() {
    //     var pdf = new jsPDF('p', 'pt', 'letter');
    //     // source can be HTML-formatted string, or a reference
    //     // to an actual DOM element from which the text will be scraped.
    //     source = $('#customers')[0];

    //     // we support special element handlers. Register them with jQuery-style 
    //     // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    //     // There is no support for any other type of selectors 
    //     // (class, of compound) at this time.
    //     specialElementHandlers = {
    //         // element with id of "bypass" - jQuery style selector
    //         '#bypassme': function(element, renderer) {
    //             // true = "handled elsewhere, bypass text extraction"
    //             return true
    //         }
    //     };
    //     margins = {
    //         top: 80,
    //         bottom: 60,
    //         left: 40,
    //         width: 522
    //     };
    //     // all coords and widths are in jsPDF instance's declared units
    //     // 'inches' in this case
    //     pdf.fromHTML(
    //         source, // HTML string or DOM elem ref.
    //         margins.left, // x coord
    //         margins.top, { // y coord
    //             'width': margins.width, // max width of content on PDF
    //             'elementHandlers': specialElementHandlers
    //         },

    //         function(dispose) {
    //             // dispose: object with X, Y of the last line add to the PDF 
    //             //          this allow the insertion of new lines after html
    //             pdf.save('Test.pdf');
    //         }, margins);
    // }



});